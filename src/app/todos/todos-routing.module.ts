import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateGroupComponent } from './components/create-group/create-group.component';
import { CreateTodoComponent } from "./components/create-todo/create-todo.component";
import { EditGroupComponent } from './components/edit-group/edit-group.component';
import { EditTodoComponent } from "./components/edit-todo/edit-todo.component";
import { ListComponent } from './components/list/list.component';

const routes: Routes = [
  {
  path: '', component: ListComponent
  },
  {
    path: 'todo/add', component: CreateTodoComponent
  },
  {
    path: 'todo/edit/:id', component: EditTodoComponent
  },
  {
    path: 'group/add', component: CreateGroupComponent
  },
  {
    path: 'group/edit/:id', component: EditGroupComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TodosRoutingModule {
}
