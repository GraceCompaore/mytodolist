import { Group } from "../group";

export class GroupImpl implements Group {
  id!: number;
  label!: string;

  constructor(label: string) {
    this.label = label;
  }

  static fromPartial(partial: Partial<Group>): GroupImpl | null {
    if(partial.label === undefined || partial.label === null) {
      return null;
    }

    const group = new GroupImpl(partial.label);
    if (partial.id !== undefined) {
      group.id = partial.id;
    }

    return group;
  }

}
