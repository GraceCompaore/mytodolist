export interface Group {
  id: number;
  label: string;
}
