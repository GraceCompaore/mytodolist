import {Component, OnInit} from '@angular/core';
import {TodoService} from "../../services/todo.service";
import {Observable, switchMap} from "rxjs";
import {Todo} from "../../models/todo";
import {TodoStatus} from "../../models/todo-status";

@Component({
  selector: 'app-list-todos',
  templateUrl: 'list-todos.components.html',
  styleUrls: ['./list-todos.component.scss']
})
export class ListTodosComponent implements OnInit {
  todoList$: Observable<Todo[]> = this.todoService.todos$
  statuses: TodoStatus[] = Object.values(TodoStatus);

  constructor(
    private todoService: TodoService
  ) {
  }

  ngOnInit(): void {
    this.todoService.getTodos().subscribe();
  }

  delete(todo: Todo) {
    this.todoService.delete(todo).subscribe();
  }

  updateStatus(todo: Todo, status: TodoStatus): void {
    this.todoService.update({...todo, status}).pipe(
      switchMap(() => this.todoService.getTodos())
    ).subscribe();
  }
}

