import { Component, OnInit } from '@angular/core';
import {GroupService} from "../../services/group.service";
import {Group} from "../../models/group";
import {Observable} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {GroupImpl} from "../../models/impl/group-impl";

@Component({
  selector: 'app-edit-group',
  templateUrl: './edit-group.component.html',
  styleUrls: ['./edit-group.component.scss']
})
export class EditGroupComponent implements OnInit {
  group$!: Observable<Group>;
  updateOnGoing: boolean = false;

  constructor(
    private groupService: GroupService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      const id = Number(params.get('id'));
      if (!isNaN(id)) {
        this.group$ = this.groupService.getById(id);
      } else {
        console.error('Invalid id');
      }
    });
  }

  submit(partialTodo: Partial<Group>) {
    this.updateOnGoing = true;
    const group = GroupImpl.fromPartial(partialTodo);
    if (group === null) {
      console.error('error');
    } else {
      this.groupService.update(group).subscribe({
        next: (data) => this.afterSubmitSuccess(data),
        error: (err) => this.afterSubmitError(err),
        complete: () => console.info('YEAH !!'),
      });
    }
  }

  private afterSubmitSuccess(data: Group) {
    this.updateOnGoing = false;
    this.router.navigate(['/']).catch(console.error);
  }

  private afterSubmitError(err: any) {
    this.updateOnGoing = false;
  }
}
