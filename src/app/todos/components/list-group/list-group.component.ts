import { Component, OnInit } from '@angular/core';
import { Observable, switchMap, mergeMap, concat } from "rxjs";
import { Group } from "../../models/group";
import { GroupService } from "../../services/group.service";
import { TodoService } from '../../services/todo.service';

@Component({
  selector: 'app-list-group',
  templateUrl: './list-group.component.html',
  styleUrls: ['./list-group.component.scss']
})
export class ListGroupComponent implements OnInit {
  groupList$: Observable<Group[]> = this.groupService.groups$

  constructor(
    private groupService: GroupService,
    private todoService: TodoService
  ) { }

  ngOnInit(): void {
    this.groupService.findAll().subscribe();
  }

  delete(group: Group) {
    this.groupService.delete(group).pipe(
      switchMap((_) => this.todoService.findByGroupId(group.id)),
      mergeMap((todoList) => {
        // Remove todo items from deleted group
        const observableList = todoList.map((todo) => {
          todo.groupIdList = todo.groupIdList.filter((id) => id !== group.id);
          return this.todoService.update(todo);
        })

        return concat(...observableList);
      })
    ).subscribe((_) => {
        // Reset display todo list
        this.todoService.getTodos().subscribe();
    });
  }

  selectGroup(group?: Group) {
    this.todoService.findByGroupId(group?.id).subscribe();
  }
}

