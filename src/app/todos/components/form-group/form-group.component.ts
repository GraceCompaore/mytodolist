import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Group } from '../../models/group';

@Component({
  selector: 'app-form-group',
  templateUrl: './form-group.component.html',
  styleUrls: ['./form-group.component.scss']
})
export class FormGroupComponent implements OnInit {
  form!: FormGroup;

  @Input() group?: Group;
  @Input() actionOnGoing: boolean = false;
  @Output() groupSubmit: EventEmitter<Partial<Group>> = new EventEmitter<Partial<Group>>();

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      label: this.fb.control(null, [Validators.required]),
    });

  }

  ngOnInit(): void {
    this.initForm();
  }

  submit() {
    if (this.group) {
      this.groupSubmit.emit({
        ...this.form.value,
        id: this.group.id
      });
    } else {
      this.groupSubmit.emit(this.form.value);
    }
  }


  resetForm() {
    this.initForm();
  }

  canSubmit() {
    return !this.actionOnGoing && this.form.valid;
  }

  private initForm() {
    if (this.group) {
      this.form.reset({
        ...this.group
      });
    } else {
      this.form.reset();
    }
  }
}
