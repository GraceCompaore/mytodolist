import {Component} from '@angular/core';
import {GroupService} from "../../services/group.service";
import {GroupImpl} from "../../models/impl/group-impl";
import {Group} from "../../models/group";
import {Router} from "@angular/router";

@Component({
  selector: 'app-create-group',
  templateUrl: './create-group.component.html',
  styleUrls: ['./create-group.component.scss']
})
export class CreateGroupComponent {
  creationOnGoing: boolean = false;

  constructor(
    private groupService: GroupService,
    private router: Router
  ) {
  }

  submit(partialGroup: Partial<Group>) {
    this.creationOnGoing = true;
    const group = GroupImpl.fromPartial(partialGroup);
    if (group === null) {
      console.error('error');
    } else {
      this.groupService.create(group).subscribe({
        next: (data) => this.afterSubmitSuccess(data),
        error: (err) => this.afterSubmitError(err),
        complete: () => console.info('YEAH !!'),
      });
    }
  }

  private afterSubmitSuccess(data: Group) {
    this.creationOnGoing = false;
    this.router.navigate(['/']).catch(console.error);
  }

  private afterSubmitError(err: any) {
    this.creationOnGoing = false;
  }
}
