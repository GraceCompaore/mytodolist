import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CreateTodoComponent} from "./components/create-todo/create-todo.component";
import {ListTodosComponent} from "./components/list-todos/list-todos.component";
import {EditTodoComponent} from "./components/edit-todo/edit-todo.component";
import {FormTodoComponent} from "./components/form-todo/form-todo.component";
import {MatInputModule} from "@angular/material/input";
import {MatSelectModule} from "@angular/material/select";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {ReactiveFormsModule} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import {TranslateModule} from "@ngx-translate/core";
import {RouterModule} from "@angular/router";
import {MatNativeDateModule, MatOptionModule} from "@angular/material/core";
import {MatListModule} from "@angular/material/list";
import {TodosRoutingModule} from "./todos-routing.module";
import { ListGroupComponent } from './components/list-group/list-group.component';
import { ListComponent } from './components/list/list.component';
import { MatIconModule } from '@angular/material/icon';
import { FormGroupComponent } from './components/form-group/form-group.component';
import { CreateGroupComponent } from './components/create-group/create-group.component';
import { EditGroupComponent } from './components/edit-group/edit-group.component';

@NgModule({
  declarations: [
    ListTodosComponent,
    CreateTodoComponent,
    EditTodoComponent,
    FormTodoComponent,
    ListGroupComponent,
    ListComponent,
    FormGroupComponent,
    CreateGroupComponent,
    EditGroupComponent,
  ],
  imports: [
    TodosRoutingModule,
    // Angular
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    // Third party
    // NgxTranslate
    TranslateModule,
    //Material
    MatInputModule,
    MatSelectModule,
    MatOptionModule,
    MatDatepickerModule,
    MatButtonModule,
    MatNativeDateModule,
    MatListModule,
    MatIconModule
  ]
})
export class TodosModule { }
