import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, tap } from "rxjs";
import { LoaderService } from 'src/app/core/services/loader.service';
import { ToolbarService } from 'src/app/core/services/toolbar.service';
import { environment } from 'src/environments/environment';
import { Group } from '../models/group';

@Injectable({
  providedIn: 'root'
})
export class GroupService {
  private baseUrl = `${environment.apiUrl}/groups`;

  private groups: BehaviorSubject<Group[]> = new BehaviorSubject<Group[]>([]);

  get groups$(): Observable<Group[]> {
    return this.groups.asObservable();
  }

  constructor(
    private http: HttpClient,
    private loaderService: LoaderService,
    private toolbarService: ToolbarService
  ) { }

  getById(id: number): Observable<Group> {
    this.loaderService.setLoadingState(true)
    return this.http.get<Group>(`${this.baseUrl}/${id}`)
      .pipe(
        tap({
          next: () => this.loaderService.setLoadingState(false),
          error: () => this.loaderService.setLoadingState(false)
        })
      );
  }

  findAll(): Observable<Group[]> {
    this.loaderService.setLoadingState(true)
    return this.http.get<Group[]>(this.baseUrl)
      .pipe(
        tap((data) => this.updateGroups(data)),
        tap({
          next: () => this.loaderService.setLoadingState(false),
          error: () => this.loaderService.setLoadingState(false)
        })
      );
  }

  create(group: Group): Observable<Group> {
    this.loaderService.setLoadingState(true)
    return this.http.post<Group>(this.baseUrl, group)
      .pipe(
        tap(data => this.updateGroups([...this.groups.value, data])),
        tap({
          next: () => this.loaderService.setLoadingState(false),
          error: () => this.loaderService.setLoadingState(false)
        })
      );
  }

  delete(group: Group) {
    this.loaderService.setLoadingState(true)
    return this.http.delete(`${this.baseUrl}/${group.id}`)
      .pipe(
        tap(() => this.updateGroups(this.groups.value.filter(t => t.id !== group.id))),
        tap({
          next: () => this.loaderService.setLoadingState(false),
          error: () => this.loaderService.setLoadingState(false)
        })
      );
  }

  update(group: Group): Observable<Group> {
    this.loaderService.setLoadingState(true)
    return this.http.put<Group>(`${this.baseUrl}/${group.id}`, group)
      .pipe(
        tap((data: Group) => {
          const todos = this.groups.value;
          const index = todos.findIndex(t => t.id === group.id);
          todos[index] = data;
          this.updateGroups(todos);
        }),
        tap({
          next: () => this.loaderService.setLoadingState(false),
          error: () => this.loaderService.setLoadingState(false)
        })
      );
  }

  private updateGroups(groupList: Group[]) {
    this.groups.next(groupList);
    this.toolbarService.setNumberOfGroups(this.groups.value.length);
  }
}
