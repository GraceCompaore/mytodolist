import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, tap, map, of } from "rxjs";
import { environment } from 'src/environments/environment';
import { LoaderService } from "src/app/core/services/loader.service";
import { ToolbarService } from "src/app/core/services/toolbar.service";
import { Todo } from "../models/todo";

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  private baseUrl = `${environment.apiUrl}/todos`;

  private todos: BehaviorSubject<Todo[]> = new BehaviorSubject<Todo[]>([])

  get todos$(): Observable<Todo[]> {
    return this.todos.asObservable();
  }

  constructor(
    private http: HttpClient,
    private loaderService: LoaderService,
    private toolbarService: ToolbarService
  ) { }

  getTodoById(id: number): Observable<Todo> {
    this.loaderService.setLoadingState(true)
    return this.http.get<Todo>(`${this.baseUrl}/${id}`)
      .pipe(
        tap({
          next: () => this.loaderService.setLoadingState(false),
          error: () => this.loaderService.setLoadingState(false)
        })
      );
  }

  findByGroupId(groupId?: number): Observable<Todo[]> {
    this.loaderService.setLoadingState(true)
    return this.http.get<Todo[]>(this.baseUrl)
      .pipe(
        map((todos: Todo[]) => {
          return todos.filter((todo: Todo) => groupId ? todo.groupIdList.includes(groupId) : todo.groupIdList.length === 0);
        }),
        tap((data) => this.updateTodos(data)),
        tap({
          next: () => this.loaderService.setLoadingState(false),
          error: () => this.loaderService.setLoadingState(false)
        })
      );
  }

  getTodos(): Observable<Todo[]> {
    this.loaderService.setLoadingState(true)
    return this.http.get<Todo[]>(this.baseUrl)
      .pipe(
        tap((data) => this.updateTodos(data)),
        tap({
          next: () => this.loaderService.setLoadingState(false),
          error: () => this.loaderService.setLoadingState(false)
        })
      );
  }

  addTodo(todo: Todo): Observable<Todo> {
    this.loaderService.setLoadingState(true)
    return this.http.post<Todo>(this.baseUrl, todo)
      .pipe(
        tap(data => this.updateTodos([...this.todos.value, data])),
        tap({
          next: () => this.loaderService.setLoadingState(false),
          error: () => this.loaderService.setLoadingState(false)
        })
      );
  }

  delete(todo: Todo) {
    this.loaderService.setLoadingState(true)
    return this.http.delete(`${this.baseUrl}/${todo.id}`)
      .pipe(
        tap(() => this.updateTodos(this.todos.value.filter(t => t.id !== todo.id))),
        tap({
          next: () => this.loaderService.setLoadingState(false),
          error: () => this.loaderService.setLoadingState(false)
        })
      );
  }

  update(todo: Todo): Observable<Todo> {
    this.loaderService.setLoadingState(true)
    return this.http.put<Todo>(`${this.baseUrl}/${todo.id}`, todo)
      .pipe(
        tap((data: Todo) => {
          const todos = this.todos.value;
          const index = todos.findIndex(t => t.id === todo.id);
          todos[index] = data;
          this.updateTodos(todos);
        }),
        tap({
          next: () => this.loaderService.setLoadingState(false),
          error: () => this.loaderService.setLoadingState(false)
        })
      );
  }

  private updateTodos(todos: Todo[]) {
    this.todos.next(todos);
    this.toolbarService.setNumberOfTodos(this.todos.value.length);
  }
}
